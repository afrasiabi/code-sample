#

## Libraries and Frameworks

TypeScript:

-   Provides robust typing solution for JavaScript
-   Reduces production bugs
-   Short learning curve and backward compatibility with JavaScript
-   Big community, good tools, and support
-   Generally, a typed language reduces test files amounts because there is no need to test inputs edge cases and null checkers and etc.

GraphQL:

-   Generally easier than REST APIs:
    -   There would only be one endpoint
    -   The data structure that the server provides is the structure that you use
    -   More natural data structure (comparing to normalized data using in REST APIs)
    -   API documentation
    -   Great tooling both in backend and frontend
-   Better performance:
    -   ability to retrieve many resources in a single request
    -   Frontend tools to provide cache

Apollo:

-   Good support of different frontend solutions
-   Easy to use API for both backend and frontend
-   Good community and documentation
-   Plenty of features and libraries
-   The powerful caching system in frontend
-   Great and easy error handling
-   Easier API for using frontend store like Redux

Styled-component:

-   Great compatibility with React component mindset
-   Completely separates styles by handling CSS, JS provided Theme and even style-related props
-   Compatibility with most of the UI libraries
-   Inline styling that allows the use of pseudos and media queries
-   Short learning curve
-   Fast-growing community and good tools like Polished and RTL generators

D3.js:
first I tried another React chart but it was so slow and disappointing in rendering huge data, so I used D3.js.
with a combination of react-hooks `useEffect` and `useRef` it looked good.

Jest:

-   It is fast and reliable
-   Easy to learn and use
-   snapshot testing feature

Enzyme: There is another option `react-testing-library` that is famous for not encouraging to have implementation details in tests. For me Enzyme was easier to setup, and I try not to use it's features to access internals of components (like .props() or wrapper.setState()) instead simulating real user intractions. But no hard feelings.

## Structure and architecture

### Frontend

I tried a fractal-like structure for files and folders in frontend. It seems like a good structure to have files in order to react tree.
I didn't separate container and UI components because in my opinion the combination of Apollo and hooks already made it clean enough and also it might be a little overkill for this small frontend.

### Backend

I didn't have many production experiences in writing backends. I just separated different logics into separated folders to make it in good order.

## Total experience

I learned and tried GraphQL and Apollo for the first time in this assignment. I focused on this part more than the others and I'm proud to make it work.

in my opinion, Things that deserve to put more effort are:

-   handling error using Apollo errors
-   Generating TypeScript types based on GraphQL types
-   Add support of `.gql` or `.graphql` file imports
-   Better UI design
