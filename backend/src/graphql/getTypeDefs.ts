import { gql } from "apollo-server-express";

// TODO: use .gql or .graphql files
// TODO: Generate TS types automatically based on Graphql types (Maybe: graphql-code-generator.com)

export const getTypeDefs = () => gql`
  type Area {
    id: Int
    name: String
    frequencies: [Float]
  }

  type Spectrum {
    x: Float
    y: Float
  }

  type Query {
    areas: [Area]
    spectrum(id: Int): [Spectrum]
  }
`;
