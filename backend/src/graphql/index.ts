import { getTypeDefs } from "./getTypeDefs";
import { getResolvers } from "./getResolvers";

export default { typeDefs: getTypeDefs(), resolvers: getResolvers() };
