import { getSpectrumByAreaId } from "../spectrum/getSpectrumByAreaId";
import { getAreas } from "../area/getAreas";

export const getResolvers = () => ({
  Query: {
    areas: () => {
      return getAreas();
    },
    spectrum: (_, { id }) => {
      return getSpectrumByAreaId(id);
    },
  },
});
