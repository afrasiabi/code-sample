import { getAreas } from "./getAreas";

jest.mock("../data/areas.json", () => [
  {
    id: 1,
    name: "x",
    frequencies: [1, 2],
  },
  {
    id: 2,
    name: "y",
    frequencies: [2, 3],
  },
]);

describe("Get Areas", () => {
  it("should return exact same data provided", () => {
    const mockedAreas = [
      {
        id: 1,
        name: "x",
        frequencies: [1, 2],
      },
      {
        id: 2,
        name: "y",
        frequencies: [2, 3],
      },
    ];
    expect(getAreas()).toStrictEqual(mockedAreas);
  });
});
