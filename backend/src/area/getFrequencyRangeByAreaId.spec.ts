import { getFrequencyRangeByAreaId } from "./getFrequencyRangeByAreaId";

jest.mock("./getAreas", () => ({
  getAreas: () => [
    {
      id: 1,
      name: "x",
      frequencies: [1, 2],
    },
    {
      id: 2,
      name: "y",
      frequencies: [2, 3],
    },
  ],
}));

describe("getFrequencyRangeByAreaId", () => {
  it("should return frequency of the selected areaId", () => {
    expect(getFrequencyRangeByAreaId(1)).toStrictEqual([1, 2]);
    expect(getFrequencyRangeByAreaId(2)).toStrictEqual([2, 3]);
  });
  it("should throw in case of wrong selected areaId", () => {
    expect(() => getFrequencyRangeByAreaId(0)).toThrow();
  });
});
