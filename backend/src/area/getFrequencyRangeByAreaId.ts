import { AreaId, getAreas } from "./getAreas";

export const getFrequencyRangeByAreaId = (areaId: AreaId): number[] => {
  let selectedArea = getAreas().find(area => area.id === areaId);
  if (selectedArea === undefined) {
    throw "Wrong areaId provided";
  }
  return selectedArea.frequencies;
};
