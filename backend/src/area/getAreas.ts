import areasJSON from "../data/areas.json";

export type AreaId = number; // posibility of adding string later;

export type Area = {
  id: AreaId;
  name: string;
  frequencies: number[];
};

const areas: Area[] = areasJSON;

export const getAreas = () => {
  return areas;
};
