import { getSpectrumByAreaId } from "./getSpectrumByAreaId";

jest.mock("../area/getFrequencyRangeByAreaId", () => ({
  getFrequencyRangeByAreaId: () => [1, 2],
}));

jest.mock("../data/spectrum.json", () => ({
  id: "x",
  date: new Date().toString(),
  spectrum: [{ x: 1, y: 2 }, { x: 2.5, y: 3 }],
}));

describe("getSpectrumByAreaId", () => {
  it("should return spectrum by areaId", () => {
    expect(getSpectrumByAreaId(1)).toStrictEqual([{ x: 1, y: 2 }]);
  });
});
