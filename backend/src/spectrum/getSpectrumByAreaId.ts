import spectrumJSON from "../data/spectrum.json";
import { AreaId } from "../area/getAreas";
import { getFrequencyRangeByAreaId } from "../area/getFrequencyRangeByAreaId";

type SpectrumShape = { x: number; y: number };
type Spectrum = SpectrumShape[];

const spectrum: Spectrum = (spectrumJSON as any).spectrum;

export const getSpectrumByAreaId = (areaId: AreaId): Spectrum => {
  const [min, max] = getFrequencyRangeByAreaId(areaId);
  return spectrum.filter((item) => item.x >= min && item.x <= max);
};
