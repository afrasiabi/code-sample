import express from "express";
import { ApolloServer } from "apollo-server-express";
import apolloServerConfig from "./graphql";

const server = new ApolloServer(apolloServerConfig);

const app = express();
server.applyMiddleware({ app });

app.listen({ port: 4000 }, () => console.log(`🚀🚀🚀 http://localhost:4000${server.graphqlPath}`));
