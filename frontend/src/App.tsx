import React from "react";
import ReactDOM from "react-dom";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import { createGlobalStyle } from "styled-components";
import reset from "styled-reset";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import { Biomarkers } from "./App/Biomarkers";
import { Theme } from "./utils/Theme";

const GlobalStyle = createGlobalStyle`
  ${reset}
  html {
    font-family: Roboto;
  }
`;

const client = new ApolloClient({
  uri: process.env.APOLLO_URL,
});

const App = () => (
  <Theme>
    <ApolloProvider client={client}>
      <GlobalStyle />
      <Biomarkers />
    </ApolloProvider>
  </Theme>
);

ReactDOM.render(<App />, document.getElementById("app"));
