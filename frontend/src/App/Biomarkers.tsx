import React, { useState } from "react";
import { Chart } from "./Biomarkers/Chart";
import { Areas } from "./Biomarkers/Areas";

export const Biomarkers = () => {
  const [selectedAreaId, setSelectedAreaId] = useState(1);

  const handleSelect = (selectedAreaId: number) => {
    setSelectedAreaId(selectedAreaId);
  };

  return (
    <>
      <Chart areaId={selectedAreaId} />
      <Areas selectedId={selectedAreaId} onSelect={handleSelect} />
    </>
  );
};
