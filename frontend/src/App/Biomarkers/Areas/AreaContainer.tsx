import styled from "styled-components";
import { lighten } from "polished";

export const AreaContainer = styled.div`
  display: flex;
  flex-direction: row;
  background-color: ${props => props.theme.primaryColor};
  color: ${props => props.theme.secondaryColor};
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const Loading = styled(AreaContainer)`
  animation-duration: 1.3s;
  animation-fill-mode: forwards;
  animation-iteration-count: infinite;
  animation-name: placeHolderShimmer;
  animation-timing-function: linear;
  background: linear-gradient(
    to right,
    ${props => props.theme.primaryColor} 8%,
    ${props => lighten(0.3, props.theme.primaryColor)} 18%,
    ${props => props.theme.primaryColor} 33%
  );
  background-size: 800px 104px;
  position: relative;
  @keyframes placeHolderShimmer {
    0% {
      background-position: -468px 0;
    }
    100% {
      background-position: 468px 0;
    }
  }
`;
