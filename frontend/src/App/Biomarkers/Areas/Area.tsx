import styled from "styled-components";

type Props = {
  selected?: boolean;
};

export const Area = styled.div<Props>`
  text-align: center;
  width: 250px;
  height: 50px;
  line-height: 50px;
  cursor: pointer;
  transition: 300ms background-color;
  &:hover {
    background-color: rgba(255, 255, 255, 0.1);
  }
  ${({ selected }) => (selected ? `background-color: rgba(255, 255, 255, 0.2);` : null)}
`;
