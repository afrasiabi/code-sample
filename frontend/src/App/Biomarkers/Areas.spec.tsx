import React from "react";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { Areas } from "./Areas";
import { Area } from "./Areas/Area";
import { Theme } from "../../utils/Theme";
import "jest-styled-components";

jest.mock("@apollo/react-hooks", () => ({
  useQuery: () => ({
    data: {
      areas: [
        {
          id: 1,
          name: "x",
          frequencies: [1, 2],
        },
        {
          id: 2,
          name: "y",
          frequencies: [2, 3],
        },
      ],
    },
  }),
}));

describe("Get Areas", () => {
  it("should match snapshot", () => {
    const handleSelect = jest.fn();
    const wrapper = shallow(<Areas onSelect={handleSelect} selectedId={1} />, {
      wrappingComponent: Theme,
    });
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it("should render areas correctly", () => {
    const handleSelect = jest.fn();
    const wrapper = shallow(<Areas onSelect={handleSelect} selectedId={1} />, {
      wrappingComponent: Theme,
    });
    expect(wrapper.find(Area).length).toBe(2);
    expect(
      wrapper
        .find(Area)
        .first()
        .text()
    ).toBe("x");
  });

  it("should select only area with selectedId", () => {
    const handleSelect = jest.fn();
    const wrapper = shallow(<Areas onSelect={handleSelect} selectedId={2} />, {
      wrappingComponent: Theme,
    });
    expect(
      wrapper
        .find(Area)
        .at(0)
        .prop("selected")
    ).toBeFalsy();
    expect(
      wrapper
        .find(Area)
        .at(1)
        .prop("selected")
    ).toBeTruthy();
  });

  it("should call onSelect with correct areaId if clicked on area", () => {
    const handleSelect = jest.fn();
    const wrapper = shallow(<Areas onSelect={handleSelect} selectedId={2} />, {
      wrappingComponent: Theme,
    });
    wrapper
      .find(Area)
      .at(0)
      .simulate("click");
    expect(handleSelect).toBeCalledTimes(1);
    expect(handleSelect).toBeCalledWith(1);
  });
});
