import React, { FunctionComponent } from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { AreaContainer, Loading } from "./Areas/AreaContainer";
import { Area } from "./Areas/Area";

const getAreasQuery = gql`
  query getAreas {
    areas {
      id
      name
      frequencies
    }
  }
`;

type Area = {
  id: number;
  name: string;
  frequency: [number, number];
};

type AreasData = {
  areas: Area[];
};

type Props = {
  onSelect: (itemId: number) => void;
  selectedId: number;
};

export const Areas: FunctionComponent<Props> = props => {
  const { loading, error, data } = useQuery<AreasData>(getAreasQuery);

  if (loading) return <Loading></Loading>;
  if (error)
    return (
      <AreaContainer>
        <Area>Server unreachable</Area>
      </AreaContainer>
    );

  return (
    <AreaContainer>
      {data &&
        data.areas.map((item, key) => (
          <Area
            selected={item.id === props.selectedId}
            onClick={() => props.onSelect(item.id)}
            key={key}
          >
            {item.name}
          </Area>
        ))}
    </AreaContainer>
  );
};
