import React, { FunctionComponent } from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { useChart } from "../../utils/useChart";
import { Spinner } from "../../components/Spinner";
import styled from "styled-components";

const getSpectrumQuery = gql`
  query getSpectrum($id: Int) {
    spectrum(id: $id) {
      x
      y
    }
  }
`;

type SpectrumShape = {
  x: number;
  y: number;
};

type SpectrumData = {
  spectrum: SpectrumShape[];
};

type Props = {
  areaId: number;
};

const SpinnerContainer = styled.div`
  width: 100%;
  height: 500px;
  text-align: center;
  line-height: 500px;
`;

export const Chart: FunctionComponent<Props> = props => {
  const { loading, data } = useQuery<SpectrumData>(getSpectrumQuery, {
    variables: { id: props.areaId },
  });

  const d3Container = useChart(data ? data.spectrum : []);

  if (loading)
    return (
      <SpinnerContainer>
        <Spinner />
      </SpinnerContainer>
    );

  return <svg className="d3-component" ref={d3Container} />;
};
