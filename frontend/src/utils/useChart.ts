import { useRef, useEffect } from "react";
import * as d3 from "d3";

type Value = {
  x: number;
  y: number;
};

export const useChart = (values: Value[]) => {
  const d3Container = useRef(null);
  useEffect(() => {
    if (values && d3Container.current) {
      // it shout that it could be null but we already checked it
      (d3Container.current as any).innerHTML = "";

      const margin = { top: 20, right: 30, bottom: 30, left: 80 };
      const height = 500;
      const width = window.innerWidth;

      const xMinAndMax = d3.extent(values, d => d.x) as [number, number]; // because it won't understand that x is always exists and it's number
      const x = d3
        .scaleLinear()
        .domain(xMinAndMax)
        .range([margin.left, width - margin.right]);

      const yMinAndMax = [0, d3.max(values, d => d.y)] as [number, number];
      const y = d3
        .scaleLinear()
        .domain(yMinAndMax)
        .range([height - margin.bottom, margin.top]);

      const xAxis = g =>
        g.attr("transform", `translate(0,${height - margin.bottom})`).call(d3.axisBottom(x));

      const yAxis = g => g.attr("transform", `translate(${margin.left},0)`).call(d3.axisLeft(y));

      const line = d3
        .line()
        .defined((d: any) => !isNaN(d.y))
        .x((d: any) => x(d.x))
        .y((d: any) => y(d.y));

      // .attr wrongly only expects string for values
      const svg = d3.select(d3Container.current).attr("viewBox", [0, 0, width, height] as any);

      svg.append("g").call(xAxis);

      svg.append("g").call(yAxis);

      svg
        .append("path")
        .datum(values)
        .attr("fill", "none")
        .attr("stroke", "steelblue")
        .attr("stroke-width", 1.5)
        .attr("stroke-linejoin", "round")
        .attr("stroke-linecap", "round")
        .attr("d", line as any);
    }
  }, [values, d3Container.current]);

  return d3Container;
};
