import React from "react";
import { ThemeProvider } from "styled-components";

const theme = {
  primaryColor: "#323d5a",
  secondaryColor: "white",
};

interface IProps {
  children: any;
}

export const Theme = (props: IProps) => (
  <ThemeProvider theme={theme}>{props.children}</ThemeProvider>
);
