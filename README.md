# Code Sample

This codebase contains [Frontend](#frontend) and [Backend](#backend) folders that they are separated. To run it on your local machine you should run both frontend and backend:

This assumes that you have `nodejs` and `yarn` installed on your machine.

## Frontend

### Step 1: Setup

```bash
$ cd ./frontend
$ yarn
```

### Step 2: Run in development mode

```bash
$ yarn start
```

### Run tests:

```bash
$ yarn test
```

## Backend

### Step 1: Setup

```bash
$ cd ./backend
$ yarn
```

### Step 2: Run in development mode

```bash
$ yarn start
```

### Run tests:

```bash
$ yarn test
```
